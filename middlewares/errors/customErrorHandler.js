const CustomError = require("../../helpers/error/CustomError");
const customErrorHander = (err, req, res, next) => {

    let customError = err;
    //console.log(err.name);

    if (err.name === "SyntaxError") {
        customError = new CustomError("Bilinmeyen Syntax", 400);
    }
    if (err.name === "ValidationError") {
        customError = new CustomError(err.message, 400);
    }

    if(err.code === 11000){
        customError = new CustomError("Kayıtlı kullanıcı bulundu. Lütfen bilgilerinizi kontrol ediniz",400);
    }

    res.status(customError.status || 500)
        .json({
            success: false,
            message: customError.message
        });
};

module.exports = customErrorHander;